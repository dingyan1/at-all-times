import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: '/index',
    children:[
      {
        path:'index',
        name:'Index',
        meta:{
          title:"每时每刻"
        },
        component:()=>import('../views/Index.vue')
      },
      {
        path:"course",
        name:"Course",
        meta:{
          title:"特色课"
        },
        component:()=>import('../views/Course.vue')
      },
      {
        path:"record",
        name:"Record",
        meta:{
          title:"约课记录"
        },
        component:()=>import('../views/Record.vue')
      },
      {
        path:"practice",
        name:"Practice",
        meta:{
          title:"练习"
        },
        component:()=>import('../views/Practice.vue')
      },
      {
        path:"my",
        name:"My",
        meta:{
          title:"个人中心"
        },
        component:()=>import('../views/My.vue')
      }
    ]
  },
  {
    path: '/contact',
    name: 'Contact',
    component: () => import( '../views/Contact.vue')
  },
  {
    path:'/detail',
    name:'Detail',
    meta:{
      title:"课程详情"
    },
    component:()=>import('../views/Index/Detail.vue')
  },
  {
    path:'/search',
    name:'Search',
    meta:{
      title:"搜索"
    },
    component:()=>import('../views/Index/Search.vue')
  },
  {
    path:'/login',
    name:'Login',
    meta:{
      title:"登录"
    },
    component:()=>import('../views/Login/Login.vue')
  },
  {
    path:'/register',
    name:'Register',
    meta:{
      title:"验证码登录"
    },
    component:()=>import('../views/Login/Register.vue')
  },
  {
    path:'/password',
    name:'Password',
    meta:{
      title:"忘记密码"
    },
    component:()=>import('../views/Login/Password.vue')
  },
  {
    path:'/user',
    name:'User',
    meta:{title:"个人信息"},
    component:()=>import('../views/My/User.vue')
  },
  {
    path:'/pass',
    name:'Pass',
    meta:{title:"设置密码"},
    component:()=>import('../views/Login/Pass.vue')
  },
  {
    path:'/name',
    name:'Name',
    meta:{title:"修改昵称"},
    component:()=>import('../views/My/Name.vue')
  },
  {
    path:'/sex',
    name:'Sex',
    meta:{title:"修改昵称"},
    component:()=>import('../views/My/Sex.vue')
  },
  {
    path:'/tdetail',
    name:'TDetail',
    meta:{title:"讲师详情"},
    component:()=>import('../views/Index/TDetail.vue')
  },
  {
    path:'/collection',
    name:'Collection',
    meta:{title:"我的收藏"},
    component:()=>import('../views/My/Collection.vue')
  },
  {
    path: "/point",
    name: "point",
    component: () => import("../views/practice/point.vue"),
    meta: {
      title: "考点专练"
    }
  },
  {
    path: "/paperPage",
    name: "paperPage",
    component: () => import("../views/practice/paperPage.vue"),
    meta: {
      title: "套卷练习"
    }
  },
  {
    path: "/exam",
    name: "exam",
    component: () => import("../views/practice/exam.vue"),
    meta: {
      title: "仿真模考"
    }
  },
  {
    path:'/oto',
    name:"oto",
    component:()=>import('../views/My/oto.vue'),
    meta:{
      title:"一对一辅导"
    }
  },
  {
    path:'/otoplan',
    name:"otoplan",
    component:()=>import('../views/My/oto/otoplan.vue'),
    meta:{
      title:"预约课程"
    }
  },
  {
    path:'/attention',
    name:"Attention",
    component:()=>import('../views/My/Attention'),
    meta:{
      title:"我的关注"
    }
  },
  {
    path:'/order',
    name:"Order",
    component:()=>import('../views/My/Order/Order.vue'),
    meta:{
      title:"订单"
    }
  },
  {
    path:'/candler',
    name:"Candler",
    component:()=>import('../views/Index/Candler.vue'),
    meta:{
      title:"学习日历"
    }
  },
  {
    path:'/mystudy',
    name:"Mystudy",
    component:()=>import('../views/My/Head/Mystudy.vue'),
    meta:{
      title:"我的学习"
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
