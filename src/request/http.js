import Request from "../util/request"
import Vue from "vue"
let that=Vue.prototype

//axiso请求轮播图
// export const banner = () => Request({
//     url: "/api/banner",
//     headers: {
//         DeviceType: 'H5'
//     }
// })

// banner轮播
export const banner = () =>that.$http.get('/banner')
//list
export const List=()=>that.$http.get('/recommend/appIndex')
//课程详情
export const corse=(obj)=>that.$http.get(`/courseInfo/basis_id=${obj}`)
//课程详情-课程大纲
export const Gang=(obj)=>that.$http.post(`/courseChapter?id=${obj}`)
//kec详情-课程评论
export const View=(obj)=>that.$http.post(`/courseComment?id=${obj}`)
//特色课
export const Course=(data)=>that.$http.get('/courseBasis',data)
//登录接口
export const Login=(data)=>Request({
    url:`/api/login`,
    method:"POST",
    params:data
})

//课程详情报名
export const apply = (data) => that.$http.post('/order/downOrder', data)
//oto列表 app/otoCourse?page=1&limit=10&attr_val_id=6&
export const oto = (data) => that.$http.get("/otoCourse", data) //Fetch("/otoCourse", data)

// //oto选择老师条件 /app/otoCourseOptions?
export const otoconditon = () => that.$http.get("/otoCourseOptions")
//预约课程
export const orderTime = (data) => that.$http.post('/teacher/invite', data)


//发送验证码
export const Sms=(data)=>that.$http.post('/smsCode',data)
//验证码登录
export const SmsLogin=(data)=>that.$http.post('/login',data)
//用户详情
export const UserInfo=()=>that.$http.get('/userInfo')
//user信息
export const UserCenter = () => that.$http.get('/getUCenterInfo')
//更换用户头像
export const Img=(data)=>that.$http.post('/public/img',data)
//城市
export const city = (data) => that.$http.get(`/sonArea/${data}`)
//提交
export const user = (data) => that.$http.put('/user', data)

//学科年级  /module/attribute/1
export const attr = () => that.$http.get('/module/attribute/1')
//讲师详情
export const teacherId = (data) => that.$http.get(`/teacher/${data}`)
//讲师关注 teacher/collect/52?
export const teacherCollect = (data) => that.$http.get(`/teacher/collect/${data}`)
//讲师列表
export const teacherList = (data) => that.$http.get('/collect', data)
//讲师列表取消关注
export const noteacher = (data) => that.$http.put(`/collect/cancel/${data}/2`)

export const teacherInfo = (data) => Request({
    url: `/api/teacher/info/${data}`,
    method: "GET"
})


//我的学习
export const mystaus = (type) => that.$http.get(`/myStudy/${type}`)
//学习日历
export const myrili = (data) => that.$http.get(`/study/live/${data}`)
// 切换日历月份 /app/study/schedule?date=2019-8-1&
export const myrlisutdy = (data) => that.$http.get(`/study/schedule?date=${data}`)
//日历 course_type==1
export const getRoom = (data) => that.$http.get(`/oto/getLiveRoomCode/${data}/0`)

export const getRoomCode = (dataid, chapterID) => that.$http.get(`/getPcRoomCode/course_id=${dataid}/chapter_id=${chapterID}`)


//课程订单
export const order = (data) => that.$http.post('/myOrder', data)
//收藏
export const collect = (data) => that.$http.post('/collect', data)
//取消收藏
export const nocollect = (data) => that.$http.put(`/collect/cancel/${data}/1`)
//收藏列表
export const collectlist = (data) => that.$http.get('/collect', data)

//余额
export const gold = () => that.$http.get('/coinBalance');
//充值
export const czgold = () => that.$http.get('/coin/coinRank')
//金额明细
export const goldmx = (data) => that.$http.get('/coin/item', data)
//优惠券 /coupon?status=0&
export const Coupon = (data) => that.$http.get('/coupon', data)
//我的学习卡 /myStudy
export const myStudy = () => that.$http.post('/myStudy')
//会员
export const Vip = () => that.$http.get('/vip/grade')

//题库选择--科目
export const pointL = () => that.$http.get("/wap/classify")
//题库选择--题库 /wap/quesBank/19
export const pointR = (data) => that.$http.get(`/wap/quesBank/${data}`)
//题库搜素 /wap/search/bank?bank=2&
export const pointSearch = (data) => that.$http.get(`/wap/search/bank?bank=${data}`)
//题库选择--选择/examPoint/21/15?
export const examPoint = (a, b) => that.$http.get(`/examPoint/${a}/${b}`)
//套餐分类 /exam/classify
export const classify = () => that.$http.get("/exam/classify")
//套卷选择 /exam/packagePractice?classify_id=8
export const packageP = (data) => that.$http.get("/exam/packagePractice", data)
//仿真考试/exam/packageSimulation
export const examlist = (data) => that.$http.get("/exam/packageSimulation", data)
//错题记录 /myExam/errorRecord?ques_source=-1
export const errorques = (data) => that.$http.post('/myExam/errorRecord', data)
//测聘记录 /examStatistics/detail
export const assess = (data) => that.$http.get("/examStatistics/detail", data)