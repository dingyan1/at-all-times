import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import '../node_modules/lib-flexible/flexible'
import Vant from 'vant';
import 'vant/lib/index.css';
import http from './util/fetch'
import plugins from './util/plugins'

Vue.use(Vant);

//绑定axios
Vue.prototype.$axios=axios //将对象挂载原型上
Vue.prototype.$http=http
Vue.prototype.$pub=plugins


//全局路由守卫,进入组件之前触发
router.beforeEach((to,from, next)=>{ 
  //判断路由是否设置title值，给组件添加标题内容
  if(to.hasOwnProperty("meta")){
    document.title = to.meta.title;
  }


  next();
});

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
